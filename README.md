# hospitalAdmission
Hospital Admission CRUD Practice in Angular 8 and Java 8 with Spring Boot.

The purpose of this app is to store admissions into a hospital for patients.

Basically a CRUD application practice using RESTApi's to communicate with frontend and backend

Utlising programming languages and technologies:

- Angular 8 
- Java 8 
- Spring Boot (Framework)
- Spring JPA (Hibernate)
- MYSQL 


